package com.list;

import com.connection.MSAccessDBConnection;
import com.persistance.StudentInfo;
import com.query.AllQuery;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class AllSelectInfo {

    public List<StudentInfo> getStusent(Connection connection) {

        StudentInfo studentInfo = null;
        List<StudentInfo> studentInfos = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(AllQuery.selectStudent());
                rs = ps.executeQuery();

                studentInfos = new ArrayList<>();

                while (rs.next()) {

                    studentInfo = new StudentInfo();

                    studentInfo.setUserName("USER_NAME");
                    studentInfo.setFirstName("FIRST_NAME");
                    studentInfo.setLastName("LAST_NAME");
                    studentInfo.setInsertBy("INSERT_BY");

                    studentInfos.add(studentInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
                studentInfos = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return studentInfos;
    }

    public static void main(String[] args) {
        AllSelectInfo all = new AllSelectInfo();
        Connection c = MSAccessDBConnection.getMySqlConnection();
        System.out.println(all.getStusent(c));
    }
}
