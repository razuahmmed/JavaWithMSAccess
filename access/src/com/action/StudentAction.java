package com.action;

import com.connection.MSAccessDBConnection;
import com.list.AllSelectInfo;
import com.model.StudentDao;
import java.sql.Connection;

public class StudentAction {

    private Connection connection = null;
    private StudentDao studentDao = null;
    private AllSelectInfo allSelectInfo = null;

    public void addStudent() {

        if (connection == null) {
            connection = MSAccessDBConnection.getMySqlConnection();
        }

        if (studentDao == null) {
            studentDao = new StudentDao();
        }

        if (connection != null) {

            int status = studentDao.newStudent(connection, "nashimkhan", "Nashim", "Khan", "razu");

            if (status > 0) {
                System.out.println("Student Added Successfully");
            } else {
                System.out.println("Student Added Failed");
            }
        }
    }

    public static void main(String[] args) {

        Connection c = MSAccessDBConnection.getMySqlConnection();
        StudentAction st = new StudentAction();
        st.studentDao = new StudentDao();

        // ADD STUDENT
//        st.addStudent();
//        
        // SELECT STUDENT
//        st.allSelectInfo = new AllSelectInfo();
//        System.out.println(st.allSelectInfo.getStusent(c));

        // UPDATE STUDENT
//        System.out.println(st.studentDao.updateStudent(c, 13, "asa", "asa", "asa", "razu"));
//        
        // DELETE STUDENT
//        System.out.println(st.studentDao.deleteStudent(c, ""));
    }
}
