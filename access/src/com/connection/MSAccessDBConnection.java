package com.connection;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class MSAccessDBConnection {

    private static volatile MSAccessDBConnection dbConnection = null;
    private static Connection connection = null;
    private static Properties properties = null;
    private static InputStream propertiesFile = null;

    private static String DRIVER = null;
    private static String URL = null;
    private static String DATABASE = null;

    private MSAccessDBConnection() {

    }

    private static MSAccessDBConnection getInstance() {

        if (dbConnection == null) {
            synchronized (MSAccessDBConnection.class) {
                if (dbConnection == null) {
                    dbConnection = new MSAccessDBConnection();
                }
            }
        }
        return dbConnection;
    }

    public static Connection getMySqlConnection() {

        try {

            properties = new Properties();

            propertiesFile = MSAccessDBConnection.class.getClassLoader().getResourceAsStream("com/connection/db.properties");
            properties.load(propertiesFile);

            DRIVER = properties.getProperty("driver");
            URL = properties.getProperty("url");
            DATABASE = properties.getProperty("database");

            connection = DriverManager.getConnection(DRIVER + URL + DATABASE);

        } catch (Exception e) {
            return null;
        }

        return connection;
    }

    public static void main(String[] args) {
        System.out.println(MSAccessDBConnection.getInstance());
    }
}
