package com.model;

import com.persistance.StudentInfo;
import com.query.AllQuery;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

public class StudentDao {

    public int newStudent(
            Connection connection,
            String userName,
            String firstName,
            String lastName,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                StudentInfo studentInfo = new StudentInfo();
                studentInfo.setUserName(userName);
                studentInfo.setFirstName(firstName);
                studentInfo.setLastName(lastName);
                studentInfo.setInsertBy(insertBy);

                List<StudentInfo> infos = new ArrayList<>();
                infos.add(studentInfo);

                ps = connection.prepareStatement(AllQuery.insertStudent(infos));
                status = ps.executeUpdate();

            } catch (Exception e) {
                e.printStackTrace();
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (Exception ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int updateStudent(
            Connection connection,
            Integer studentId,
            String userName,
            String firstName,
            String lastName,
            String updateBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                StudentInfo studentInfo = new StudentInfo();
                studentInfo.setStudentId(studentId);
                studentInfo.setUserName(userName);
                studentInfo.setFirstName(firstName);
                studentInfo.setLastName(lastName);
                studentInfo.setUpdateBy(updateBy);

                List<StudentInfo> infos = new ArrayList<>();
                infos.add(studentInfo);

                ps = connection.prepareStatement(AllQuery.updateStudent(infos));
                status = ps.executeUpdate();

            } catch (Exception e) {
                e.printStackTrace();
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (Exception ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int deleteStudent(
            Connection connection,
            String userName) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(AllQuery.deleteStudent());
                status = ps.executeUpdate();

            } catch (Exception e) {
                e.printStackTrace();
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (Exception ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }
}
