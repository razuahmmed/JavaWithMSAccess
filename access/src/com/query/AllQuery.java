package com.query;

import com.persistance.StudentInfo;
import java.util.List;

public class AllQuery {

    public static String insertStudent(List<StudentInfo> studentInfos) {

        String insertStatement = "";

        for (StudentInfo info : studentInfos) {

            insertStatement = " INSERT INTO "
                    + " STUDENT_INFO ( "
                    + " USER_NAME, "
                    + " FIRST_NAME, "
                    + " LAST_NAME, "
                    + " INSERT_BY, "
                    + " INSERT_DATE "
                    + " )VALUES( "
                    + " '" + info.getUserName() + "', "
                    + " '" + info.getFirstName() + "', "
                    + " '" + info.getLastName() + "', "
                    + " '" + info.getInsertBy() + "', "
                    + " CURRENT_TIMESTAMP)";
        }

        return insertStatement;
    }

    public static String selectStudent() {
        return " SELECT USER_NAME, FIRST_NAME, LAST_NAME, INSERT_BY FROM STUDENT_INFO ";
    }

    public static String updateStudent(List<StudentInfo> studentInfos) {

        String updateStatement = "";

        for (StudentInfo info : studentInfos) {

            updateStatement = " UPDATE "
                    + " STUDENT_INFO "
                    + " SET "
                    + " USER_NAME = '" + info.getUserName() + "', "
                    + " FIRST_NAME = '" + info.getFirstName() + "', "
                    + " LAST_NAME = '" + info.getLastName() + "', "
                    + " UPDAYE_BY = '" + info.getUpdateBy() + "', "
                    + " UPDATE_DATE = CURRENT_TIMESTAMP "
                    + " WHERE "
                    + " STUDENT_ID = '" + info.getStudentId() + "' ";
        }

        return updateStatement;
    }

    public static String deleteStudent() {
        return " DELETE FROM STUDENT_INFO ";
    }
}
